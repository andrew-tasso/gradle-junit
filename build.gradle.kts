plugins {
    kotlin("jvm") version "1.3.50"
    id("java-gradle-plugin")
    id("com.gradle.plugin-publish") version "0.10.0"
}

group = "dev.tasso"
version = "0.1"

repositories {
    jcenter()
}

dependencies {

    implementation(platform("org.jetbrains.kotlin:kotlin-bom:1.3.50"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.3.50")
    testImplementation("org.jetbrains.kotlin:kotlin-test:1.3.50")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5:1.3.50")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.4.0")
    testRuntimeOnly(group = "org.junit.jupiter", name = "junit-jupiter-engine", version = "5.5.2")

}

tasks.getByName<Wrapper>("wrapper") {
    gradleVersion = "5.6.2"
    distributionType = Wrapper.DistributionType.ALL
}

gradlePlugin {
    val gradleGit by plugins.creating {
        id = "dev.tasso.gradle-junit"
        displayName = "Gradle Test Task Configurator"
        description = "Gradle Plugin to assist in providing opinionated configuration of test tasks"
        implementationClass = "dev.tasso.gradlejunit.GradleJunitPlugin"
    }
}

pluginBundle {
    website = "https://gitlab.com/andrew.tasso/gradle-junit"
    vcsUrl = "https://gitlab.com/andrew.tasso/gradle-junit.git"
    tags = listOf("junit", "test", "testing", "configuration")
}

// Add a source set for the functional test suite
val functionalTestSourceSet = sourceSets.create("functionalTest") {}

gradlePlugin.testSourceSets(functionalTestSourceSet)

configurations.named("functionalTestImplementation").extendsFrom(configurations.named("testImplementation").get())
configurations.named("functionalTestRuntimeOnly").extendsFrom(configurations.named("testRuntimeOnly").get())

val functionalTest by tasks.creating(Test::class) {
    testClassesDirs = functionalTestSourceSet.output.classesDirs
    classpath = functionalTestSourceSet.runtimeClasspath
}

val check by tasks.getting(Task::class) {
    dependsOn(functionalTest)
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging.showStandardStreams = true
}

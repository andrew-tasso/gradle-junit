package dev.tasso

import java.io.File
import org.gradle.testkit.runner.GradleRunner
import kotlin.test.Test

class GradleJunitPluginFunctionalTest {

    @Test fun `can run task`() {

        val projectDir = File("build/functionalTest")
        projectDir.mkdirs()
        projectDir.resolve("settings.gradle").writeText("")
        projectDir.resolve("build.gradle").writeText(
    """
            plugins {
                id('dev.tasso.gradle-junit')
            }
        """)

        val runner = GradleRunner.create()
        runner.forwardOutput()
        runner.withPluginClasspath()
        runner.withProjectDir(projectDir)
        runner.build();
    }
}

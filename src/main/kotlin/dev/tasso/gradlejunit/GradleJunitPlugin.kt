package dev.tasso.gradlejunit

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test

class GradleJunitPlugin: Plugin<Project> {

    companion object {
        const val JUNIT_VERSION = "5.6.2"
    }

    override fun apply(theProject: Project) {

        theProject.logger.debug("Applying Gradle JUnit plugin")

        this.configureRequiredPlugins(theProject)
        this.configureDependencies(theProject)
        this.configureTestTasks(theProject)

        theProject.logger.debug("Gradle JUnit plugin applied!")

    }

    private fun configureRequiredPlugins(theProject: Project) {

        theProject.logger.debug("Applying required plugins")

        //Apply the java plugin so that the dependency configurations we're modifying will already be in place
        theProject.pluginManager.apply("java")

        theProject.logger.debug("Plugins applied!")

    }

    private fun configureDependencies(theProject: Project) {

        theProject.logger.debug("Configuring project test dependencies")

        theProject.configurations.named("testImplementation"){
            it.dependencies.add(theProject.dependencies.create("org.junit.jupiter:junit-jupiter-api:$JUNIT_VERSION"))
        }

        theProject.configurations.named("testRuntimeOnly") {
            it.dependencies.add(theProject.dependencies.create("org.junit.jupiter:junit-jupiter-engine:$JUNIT_VERSION"))
        }

        theProject.logger.debug("Project test dependencies configuration complete!")

    }

    private fun configureTestTasks(theProject: Project) {

        theProject.logger.debug("Configuring test tasks")

        theProject.tasks.withType(Test::class.java).configureEach {
            it.useJUnitPlatform()
            it.testLogging.showStandardStreams = true
        }

        theProject.logger.debug("Test task configuration complete!")

    }

}
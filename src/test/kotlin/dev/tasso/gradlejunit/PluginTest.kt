package dev.tasso.gradlejunit

import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.BehaviorSpec
import org.gradle.api.internal.tasks.testing.junitplatform.JUnitPlatformTestFramework
import org.gradle.api.tasks.testing.Test
import org.gradle.testfixtures.ProjectBuilder

class PluginTest : BehaviorSpec({

    Given("a base project") {

        val project = ProjectBuilder.builder().build()

        When ("applying the plugin") {

            project.pluginManager.apply("dev.tasso.gradle-junit")

            Then("the plugin is applied to the project") {

                project.plugins.getPlugin(GradleJunitPlugin::class.java) shouldNotBe null

            }
            Then("the JUnit 5 Platform dependencies are added to the test dependency configurations") {

                project.configurations.getByName("testImplementation").dependencies.find {
                    it.group == "org.junit.jupiter" && it.name == "junit-jupiter-api" && it.version == "5.6.2"
                } shouldNotBe null

                project.configurations.getByName("testRuntimeOnly").dependencies.find {
                    it.group == "org.junit.jupiter" && it.name == "junit-jupiter-engine" && it.version == "5.6.2"
                } shouldNotBe null

            }
            Then("all test tasks are configured to use JUnit Platform") {

                //TODO make sure that some test tasks actually exist
                project.tasks.withType(Test::class.java).find{ it.testFramework !is JUnitPlatformTestFramework } shouldBe null

            }
            Then("all test tasks are configured to show standard streams to the JVM console") {

                //TODO make sure that some test tasks actually exist
                project.tasks.withType(Test::class.java).find { !it.testLogging.showStandardStreams } shouldBe null

            }
        }
    }
})